const Sequelize = require('sequelize');
const sequelize = require('../config/database');

  const Checker = sequelize.define('checker', {
    id_master_checker: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    id_checker: {
      type: Sequelize.STRING
    },
    id_validator1: {
        type: Sequelize.STRING
    },
    id_validator2: {
        type: Sequelize.STRING
    },
    id_cabang: {
        type: Sequelize.STRING
    },
    created_date: {
      type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    updated_date: {
      type: Sequelize.STRING
    },
    updated_by: {
        type: Sequelize.STRING
    },
    deleted: {
      type: Sequelize.STRING
    }
  },{
    freezeTableName: true,
    timestamps: false,
  }
  
  );

  module.exports = Checker;
  