const Sequelize = require('sequelize');
const sequelize = require('../config/database');


  const Admin = sequelize.define('admin_ertracking', {
    username: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    password: {
      type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    updated_date: {
        type: Sequelize.STRING
    },
    updated_by: {
        type: Sequelize.STRING
    }
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = Admin;
  