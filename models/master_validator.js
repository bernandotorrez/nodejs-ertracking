const Sequelize = require('sequelize');
const sequelize = require('../config/database');


  const MasterValidator = sequelize.define('master_validator', {
    id_validator: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    nama_validator: {
      type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    updated_date: {
        type: Sequelize.STRING
    },
    updated_by: {
        type: Sequelize.STRING
    },
    deleted: {
        type: Sequelize.STRING
    }
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = MasterValidator;
  