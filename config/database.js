if(process.env.NODE_ENV=='development'){
  var config = {
    user: 'IT_APP',
    password: 'P@ssw0rd',
    server: '172.16.2.49',
    database: 'BAFNETAPP' 
  };
} else {
  var config = {
    user: 'IT_APP',
    password: 'P@ssw0rd',
    server: '172.16.2.49',
    database: 'BAFNETAPP' 
  };
}

const Sequelize = require('sequelize');

const sequelize = new Sequelize(config.database, config.user, config.password, {
    host: config.server,
    dialect: 'mssql',
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  });
  

module.exports = sequelize;