const Sequelize = require('sequelize');
const sequelize = require('../config/database');


  const Departemen = sequelize.define('department_list', {
    id_departemen: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    Title: {
      type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    updated_date: {
        type: Sequelize.STRING
    },
    updated_by: {
        type: Sequelize.STRING
    },
    deleted: {
        type: Sequelize.STRING
    }
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = Departemen;
  