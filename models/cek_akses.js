const Sequelize = require('sequelize');
const sequelize = require('../config/database');


  const CekAkses = sequelize.define('cek_akses', {
    id_akses: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    nik: {
      type: Sequelize.STRING
    },
    nama: {
        type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    updated_date: {
        type: Sequelize.STRING
    },
    updated_by: {
        type: Sequelize.STRING
    },
    deleted: {
        type: Sequelize.STRING
    },
    active: {
        type: Sequelize.STRING
    }
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = CekAkses;
  