const Sequelize = require('sequelize');
const sequelize = require('../config/database');


  const TipeER = sequelize.define('tipe_er_list', {
    id_tipe: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    TIPE_ER: {
      type: Sequelize.STRING
    },
    DESKRIPSI: {
        type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    updated_date: {
        type: Sequelize.STRING
    },
    updated_by: {
        type: Sequelize.STRING
    },
    deleted: {
        type: Sequelize.STRING
    },
    er_type: {
      type: Sequelize.STRING
    }
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = TipeER;
  