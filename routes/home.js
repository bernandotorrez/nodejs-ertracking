var express = require('express');
var router = express.Router();
var constant = require('../config/constants');
var general_function = require('../config/function');
var soap = require('soap');
var CekAksesModel = require('../models/cek_akses');
const sequelize = require('../config/database');

// Cek Session apakah ada atau tidak, kalo tidak oper ke Halaman Login
var check_user_session = require('../middleware/check_user_session');

router.all('*', check_user_session, (req, res, next) => {
    next()
})
// Cek Session apakah ada atau tidak, kalo tidak oper ke Halaman Login

// Halaman Index
router.get('/', function (req, res, next) {
    //res.send(req.session);

    var {
        level,
        cost_control,
        nama
    } = req.session

    res.render('layout', {
        title: constant.title,
        nama: nama,
        content: '/user/home',
        title_page: 'INDEX',
        body_class: 'no-skin',
        
        level: level,
        url: req.url,
        menu: general_function.cek_level(level),
        cost_control: cost_control
    });
});
// Halaman Index

// Halaman Profile
router.get('/profil', (req, res, next) => {

    let NO_KARYAWAN = req.session.nik;
    let {
        level,
        cost_control,
        nama,
        login_time
    } = req.session;

    getDataEmployeeNIK(NO_KARYAWAN, (result) => {
        result['login_time'] = login_time; //fungsi nya kyk array push, memasukkan value ke dalam array associative
        res.render('layout', {
            title: constant.title,
            nama: nama,
            content: '/user/profile',
            title_page: 'PROFILE',
            body_class: 'no-skin',
            
            data_employee: result,
            level: level,
            url: req.url,
            menu: general_function.cek_level(level),
            cost_control: cost_control
        })
    })

})
// Halaman Profile

// Halaman Headquarter untuk Searching No ER PR
router.get('/headquarter', (req, res, next) => {
    var full_url = `${req.protocol}://${req.get('host')}/`;
    var {
        level,
        cost_control,
        nama,
        nik,
        departemen
    } = req.session;

    var query_master_checker = `select id_checker, nama_checker from view_master_checker order by nama_checker asc`;
    var query_validator = `select id_validator, nama_validator from view_master_validator order by nama_validator asc`;
    var query_departemen = `select id_departemen, Title from view_master_departement order by Title asc`;
    var query_tipe_er = `select id_tipe, TIPE_ER from view_tipe_er order by TIPE_ER asc`;
    var query_tac = `select id_tac, nama_tac from view_master_tac order by nama_tac asc`;

    const data_master_checker = sequelize.query(query_master_checker, {
        type: sequelize.QueryTypes.SELECT
    });
    const data_validator = sequelize.query(query_validator, {
        type: sequelize.QueryTypes.SELECT
    });
    const data_departemen = sequelize.query(query_departemen, {
        type: sequelize.QueryTypes.SELECT
    });
    const data_tipe_er = sequelize.query(query_tipe_er, {
        type: sequelize.QueryTypes.SELECT
    });
    const data_tac = sequelize.query(query_tac, {
        type: sequelize.QueryTypes.SELECT
    });

    Promise
        .all([data_master_checker, data_validator, data_departemen, data_tipe_er, data_tac])
        .then(responses => {
            // responses[0]; data_master_checker
            // responses[1]; data_master_validator
            // responses[2]; data_list_checker
            console.log(responses[4])
            res.render('layout', {
                title: constant.title,
                nama: nama,
                nik: nik,
                content: '/user/headquarter',
                title_page: 'INDEX',
                body_class: 'no-skin',
                
                level: level,
                url: req.url,
                full_url: full_url,
                data_master_checker: responses[0],
                data_validator: responses[1],
                data_departemen: responses[2],
                data_tipe_er: responses[3],
                data_tac: responses[4],
                menu: general_function.cek_level(level),
                cost_control: cost_control,
                departemen: departemen
            })
        })
        .catch(err => {
            next(err)
        });
})
// Halaman Headquarter untuk Searching No ER PR

// Halaman Branch untuk Searching No ER PR
router.get('/branch', (req, res, next) => {
    var full_url = `${req.protocol}://${req.get('host')}/`;
    var {
        level,
        cost_control,
        nama,
        nik,
        departemen
    } = req.session;

    var query_master_checker = `select id_checker, nama_checker from view_master_checker order by nama_checker asc`;
    var query_validator = `select id_validator, nama_validator from view_master_validator order by nama_validator asc`;
    var query_departemen = `select id_departemen, Title from view_master_departement order by Title asc`;
    var query_tipe_er = `select id_tipe, TIPE_ER from view_tipe_er order by TIPE_ER asc`;
    var query_tac = `select id_tac, nama_tac from view_master_tac order by nama_tac asc`;

    const data_master_checker = sequelize.query(query_master_checker, {
        type: sequelize.QueryTypes.SELECT
    });
    const data_validator = sequelize.query(query_validator, {
        type: sequelize.QueryTypes.SELECT
    });
    const data_departemen = sequelize.query(query_departemen, {
        type: sequelize.QueryTypes.SELECT
    });
    const data_tipe_er = sequelize.query(query_tipe_er, {
        type: sequelize.QueryTypes.SELECT
    });
    const data_tac = sequelize.query(query_tac, {
        type: sequelize.QueryTypes.SELECT
    });

    Promise
        .all([data_master_checker, data_validator, data_departemen, data_tipe_er, data_tac])
        .then(responses => {
            // responses[0]; data_master_checker
            // responses[1]; data_master_validator
            // responses[2]; data_list_checker
            console.log(data_tac)
            res.render('layout', {
                title: constant.title,
                nama: nama,
                nik: nik,
                content: '/user/branch',
                title_page: 'INDEX',
                body_class: 'no-skin',
                
                level: level,
                url: req.url,
                full_url: full_url,
                data_master_checker: responses[0],
                data_validator: responses[1],
                data_departemen: responses[2],
                data_tipe_er: responses[3],
                data_tac: responses[4],
                menu: general_function.cek_level(level),
                cost_control: cost_control,
                departemen: departemen
            })
        })
        .catch(err => {
            next(err)
        });
})
// Halaman Branch untuk Searching No ER PR

// Halaman Report
router.get('/report', (req, res, next) => {
    var {
        level,
        cost_control,
        nama,
        nik,
        departemen
    } = req.session;

    res.render('layout', {
        title: constant.title,
        nama: nama,
        content: '/user/report',
        title_page: 'REPORT',
        body_class: 'no-skin',
        
        level: level,
        url: req.url,
        menu: general_function.cek_level(level),
        cost_control: cost_control
    })

})

router.post('/report', (req, res, next) => {
    var {
        level,
        cost_control,
        nama,
        nik,
        departemen
    } = req.session;

    let {
        tgl_awal,
        tgl_akhir
    } = req.body;

    fsGetDataHQ(tgl_awal, tgl_akhir, (result) => {
        res.render('layout', {
            title: constant.title,
            nama: nama,
            content: '/user/report_post',
            title_page: 'REPORT',
            body_class: 'no-skin',
            
            level: level,
            url: req.url,
            menu: general_function.cek_level(level),
            cost_control: cost_control,
            tgl_awal: tgl_awal,
            tgl_akhir: tgl_akhir,
            data_report: result
        })
    })
    
})
// Halaman Report

function fsGetDataHQ(tgl_awal, tgl_akhir, callback) {
    const url = constant.url_ws;

    let args = {
        requestinsertgerdetail: {
            INSERT_DATE: tgl_awal,
            INSERT_DATE2: tgl_akhir
        }
    }
    soap.createClientAsync(url).then((client) => {

        client.fsGetDataHQ(args, function (err, result, rawResponse, soapHeader, rawRequest) {
            let callback_data;
            if (result.requestGETdetail!=null) {
                callback_data = result.requestGETdetail.ArrayData;
            } else {
                callback_data = {
                    'PR_NUMBER': '',
                    'ER_NUMBER': '',
                    'VALIDATOR': '',
                    'VALIDATOR2': '',
                    'CHECKER': '',
                    'KODECABANG': '',
                    'DEPARTEMENT': '',
                    'TIPE_ER': '',
                    'PERIODE': '',
                    'TANGGAL_INCOMING': '',
                    'TANGGAL_PROSES_CHECKER': '',
                    'STATUS': '',
                    'REMARKS': '',
                    'TANGGAL_INCOMING_FINANCE': '',
                    'ACTIVE': '',
                    'INSERT_DATE': '',
                    'TYPE_PR_ER_NUMBER': '',
                    'APPROVER_DATE': '',
                    'NAME_OF_APPROVERD': '',
                    'SLA_PR': '',
                    'SLA_PO': '',
                    'SLA_ER_DATE': '',
                    'SLA_KIRIM': '',
                    'SLA_CABANG': '',
                    'ACCT': '',
                    'SLA_FIN': '',
                    'SLA_HQ': '',
                    'INVOICE_NUM': ''
                }
            }

            callback(callback_data)
        })
    })
}

function adFindUser(nik, callback) {

    ad.findUser(nik, function (err, user) {

        if (err) {
            callback(err)
            return;
        }

        if (!user) {
            callback('User: ' + nik + ' not found.');
        } else {
            callback(user)
        }
    });
}

function adFindUsers(cn, callback) {
    ad.findUsers(cn, true, function (err, results) {
        if ((err) || (!results)) {
            callback('ERROR: ' + JSON.stringify(err));
            return;
        }

        callback(results[0])

    });
}

function getDataEmployeeNIK(no_karyawan, callback) {
    const url = constant.url_ws_employee;

    let args1 = {
        docGetEmployeeDataDynRequest: {
            NO_KARYAWAN: no_karyawan
        }
    }

    soap.createClientAsync(url).then((client) => {

        client.getEmployeeData1Dyn(args1, function (err, result, rawResponse, soapHeader, rawRequest) {

            console.log(err)

            if (err) {
                callback(err)
                return
            } else {
                if (!result) {
                    callback('Data tidak Ditemukan')

                } else {
                    var {
                        NO_KARYAWAN,
                        NAMA,
                        DIVISI,
                        DEPARTEMEN,
                        JOB,
                        POSISI,
                        CABANG
                    } = result.docGetEmployeeDataDynResponse;
                    var data = {
                        'no_karyawan': NO_KARYAWAN,
                        'nama': NAMA,
                        'cabang': CABANG,
                        'divisi': DIVISI,
                        'departemen': DEPARTEMEN,
                        'job': JOB,
                        'posisi': POSISI
                    }
                }
                callback(data)
            }
        })
    })

}

module.exports = router