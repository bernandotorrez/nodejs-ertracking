var CekAksesModel = require('../models/cek_akses');

module.exports = function (req, res, next) { 
    // 401 Unauthorized
    // 403 Forbidden 

    var {
        nik
    } = req.session

    if (!req.session.level || req.session.level == '') {

        // delete session object
        return res.status(403).send('Access Denied');

    } else {
        CekAksesModel.findOne({
                where: {
                    nik: `${nik}`,
                    deleted: '0'
                }
            })
            .then(result => {
                if (!result) {
                    return next()
                } else {
                    if (result.active == '0') {
                        return res.redirect('/forbidden')
                    } else {
                        return next()
                    }
                }

            })
    }
  }