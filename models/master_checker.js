const Sequelize = require('sequelize');
const sequelize = require('../config/database');


  const MasterChecker = sequelize.define('master_checker', {
    id_checker: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    nama_checker: {
      type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.STRING
    },
    created_by: {
        type: Sequelize.STRING
    },
    updated_date: {
        type: Sequelize.STRING
    },
    updated_by: {
        type: Sequelize.STRING
    },
    deleted: {
        type: Sequelize.STRING
    }
  },{
    freezeTableName: true,
    timestamps: false
  }
  
  );

  module.exports = MasterChecker;
  